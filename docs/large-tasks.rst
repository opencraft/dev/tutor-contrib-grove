Files under ``tutorgrove/templates/grove/tasks`` are added to
``CLI_DO_INIT_TASKS``, where they are processed as templates and their
output is run as a shell command. Those templates are rendered late,
only right before execution, at ``tutor.commands.jobs:do_callback()``,
then passed as a single argument to ``sh -e -c``. Some templates,
however, may generate scripts that are too large to be passed as a
command line argument and will cause an error.

To deal with that, we add a callback to ``CLI_DO_INIT_TASKS`` that will
render the template and split the output into multiple items. This
output will be rendered as a template again by ``do_callback()`` so
it is necessary to escape it.

This is only applied to templates with a filename ending in ``.large``.
Lines consisting of:

.. code:: bash

    #grove-large-task-break

indicate where it is safe to break up the generated shell script.
See ``tutorgrove.plugin:_large_cli_task()`` for the implementation.
