from importlib import resources

from tutor import hooks

from ...__about__ import __version__
from ...hook_utils import bootstrap_plugin

try:
    from tutorforum.hooks import FORUM_ENV
except ImportError:
    FORUM_ENV = None

config = {
    # Add here your new settings
    "defaults": {
        "VERSION": __version__,

        # Enable/disable shared Elasticsearch
        "ENABLE_SHARED_ELASTICSEARCH": False,
    },
    # Danger zone! Add here values to override settings from Tutor core or other plugins.
    "overrides": {
        # The list of indexes is defined in:
        # https://github.com/openedx/course-discovery/blob/master/course_discovery/settings/base.py
        # We need to keep a copy of this list so that we can prefix the index names when using shared ES.
        "DISCOVERY_INDEX_OVERRIDES": {
            "course_discovery.apps.course_metadata.search_indexes.documents.course": "{{ ELASTICSEARCH_INDEX_PREFIX }}course",
            "course_discovery.apps.course_metadata.search_indexes.documents.course_run": "{{ ELASTICSEARCH_INDEX_PREFIX }}course_run",
            "course_discovery.apps.course_metadata.search_indexes.documents.learner_pathway": "{{ ELASTICSEARCH_INDEX_PREFIX }}learner_pathway",
            "course_discovery.apps.course_metadata.search_indexes.documents.person": "{{ ELASTICSEARCH_INDEX_PREFIX }}person",
            "course_discovery.apps.course_metadata.search_indexes.documents.program": "{{ ELASTICSEARCH_INDEX_PREFIX }}program",
        }
    },
    "global_defaults": {
        "ELASTICSEARCH_INDEX_PREFIX": "",
        "ELASTICSEARCH_HTTP_AUTH": "",
        "ELASTICSEARCH_CA_CERT_PEM": ""
    }
}

bootstrap_plugin("elasticsearch", config)

if FORUM_ENV:
    @FORUM_ENV.add()
    def _add_forum_env_vars(env_vars):
        env_vars.update(
            {
                "SEARCH_SERVER": "{{ ELASTICSEARCH_SCHEME }}://{{ ELASTICSEARCH_HTTP_AUTH and (ELASTICSEARCH_HTTP_AUTH + '@') }}{{ ELASTICSEARCH_HOST }}:{{ ELASTICSEARCH_PORT }}",
                "ELASTICSEARCH_INDEX_PREFIX": '{{ ELASTICSEARCH_INDEX_PREFIX|default("") }}',
            }
        )
        return env_vars
