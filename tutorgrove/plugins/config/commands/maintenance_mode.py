import click
from tutor import config as tutor_config
from tutor import env as tutor_env
from tutor import utils


@click.command(help="Enable/disable maintenance mode")
@click.option("--enable/--disable")
@click.pass_context
def maintenance_mode(context: click.Context, enable: bool):
    """
    Enables/disables maintenance mode.

    Replaces the ingress resources for the instance with ones
    meant for maintenance.
    """
    instance_namespace = tutor_config.load_full(context.obj.root).get("K8S_NAMESPACE")

    if not instance_namespace:
        click.echo(
            "The key K8S_NAMESPACE does not exist in the instance config.",
            err=True,
        )
        raise click.Abort()

    if enable:
        maintenance_manifest = tutor_env.pathjoin(
            context.obj.root, "plugins/grove/k8s/maintenance-mode.yml"
        )
        utils.kubectl(
            "delete",
            "ingress",
            "--ignore-not-found=true",
            f"-n{instance_namespace}",
            "ingress",
        )
        utils.kubectl("apply", "-f", maintenance_manifest)
        click.echo(
            "Maintenance mode is now enabled. Please check that the correct UI is served by your installation."
        )
    else:
        maintenance_namespace = f"{instance_namespace}-maintenance"
        ingress_manifest = tutor_env.pathjoin(
            context.obj.root, "plugins/grove/k8s/ingress.yml"
        )
        utils.kubectl(
            "delete",
            "namespace",
            "--ignore-not-found=true",
            maintenance_namespace,
        )
        utils.kubectl("apply", "-f", ingress_manifest)
        click.echo(
            "Maintenance mode is now disabled. Please check that the correct UI is served by your installation."
        )
