from tutor import hooks

from .commands.grove_config_save import grove_config_save
from .commands.maintenance_mode import maintenance_mode
from ...__about__ import __version__
from ...hook_utils import bootstrap_plugin

################# Configuration
config = {
    # Add here your new settings
    "defaults": {
        "VERSION": __version__,
        # If you want to run the MFE's behind a CDN
        # This is the origin that your CDN will proxy to.
        "MFE_CDN_ORIGIN": "",

        # Extra origins/hosts which need to be allowed and
        # whitelisted for CORS
        "ALLOW_EXTRA_ORIGINS": [],

        # Set the static URL for LMS/CMS respectively. Leave empty for default.
        "LMS_STATIC_URL": "",
        "CMS_STATIC_URL": "",

        # Set spec.revisionHistoryLimit for any
        # of the default deployments created by tutor.
        "REVISION_HISTORY_LIMIT": 10,

        # Set to True to host static files on S3
        "ENABLE_S3_STATIC_FILES": False,

        # The bucket name to use for S3 static files
        "S3_STATIC_FILES_BUCKET_NAME": "{{S3_STORAGE_BUCKET}}",

        # Enable/disable the Celery beat pods
        "ENABLE_CELERY_BEAT": False,

        "MAINTENANCE_S3_BUCKET_ROOT_URL": "",
        "SERVER_ERROR_S3_BUCKET_ROOT_URL": "",

        # If the comprehensive theme used by the Grove instance contains a npm brand package,
        # set this variable to the name of the package so that it gets installed in the MFE environment.
        "COMPREHENSIVE_THEME_BRAND_PACKAGE_NAME": "",

        "CADDY_VOLUME_SIZE": "",
        "ELASTICSEARCH_VOLUME_SIZE": "",
        "REDIS_VOLUME_SIZE": "",

        # Enable automatic waffle flag creation
        #
        # Example:
        #   WAFFLE_FLAGS: [
        #       {
        #           "name": "course_experience.enable_about_sidebar_html",
        #           "everyone": true,
        #       },
        #   ]
        "WAFFLE_FLAGS": [],

        # Additional settings
        "COMMON_ENV_FEATURES": "",
        "COMMON_SETTINGS": "",

        "XBLOCK_HANDLER_TOKEN_KEYS": [],

        "LMS_ENV": "",
        "LMS_ENV_FEATURES": "",
        "LMS_PRODUCTION_SETTINGS": "",

        "CMS_ENV": "",
        "CMS_ENV_FEATURES": "",
        "CMS_PRODUCTION_SETTINGS": "",

        "MFE_LMS_COMMON_SETTINGS": "",

        # Open edX auth settings
        "OPENEDX_AUTH": "",

        # Enable cron jobs
        #
        # Example:
        #   CRON_JOBS: [
        #       {
        #           "name": "run-aggregator-service",
        #           "schedule": "*/10 * * * *",
        #           "script": "./manage.py cms run_aggregator_service"
        #       },
        #       {
        #           "name": "hello-world",
        #           "schedule": "*/20 * * * *",
        #           "script": 'echo "Hello world"',
        #       }
        #   ]
        "CRON_JOBS": [],
        # AWS endpoint URL
        "AWS_S3_ENDPOINT_URL": "",
    },
}

def is_plugin_loaded_grove(plugin_name: str) -> bool:
    """
    Check if the provided plugin is loaded.
    """

    return plugin_name in hooks.Filters.PLUGINS_LOADED.iterate()

hooks.Filters.ENV_TEMPLATE_VARIABLES.add_item(
    ("is_plugin_loaded_grove", is_plugin_loaded_grove),
)

bootstrap_plugin("config", config)

hooks.Filters.CLI_COMMANDS.add_item(maintenance_mode)
hooks.Filters.CLI_COMMANDS.add_item(grove_config_save)

# Disable patches in the Dockerfile, because they don't apply cleanly
hooks.Filters.ENV_PATCHES.add_item(
    (
        "openedx-dockerfile-git-patches-default",
        "#"
    )
)
