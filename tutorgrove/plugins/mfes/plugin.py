from tutor import config as tutor_config, hooks
from tutormfe.hooks import MFE_APPS
from tutormfe.plugin import get_mfes

from ...__about__ import __version__
from ...hook_utils import merge_dict, bootstrap_plugin

config = {
    "defaults": {
        "VERSION": __version__,
        # MFEs listed here will be disabled
        "DISABLED_MFES": [],
        # MFE list to be added/updated
        "NEW_MFES": {},
    },
}

bootstrap_plugin("mfes", config)


@hooks.Actions.PROJECT_ROOT_READY.add()
def _modify_mfes(root: str):
    config = tutor_config.load_minimal(root)
    disabled_mfes = config.get('GROVE_DISABLED_MFES', [])
    new_mfes = config.get('GROVE_NEW_MFES', {})
    get_mfes and get_mfes.cache_clear()

    @MFE_APPS.add()
    def _add_remove_mfe(mfes):
        for key in disabled_mfes:
            if key in mfes:
                mfes.pop(key)
        mfes = merge_dict(mfes, new_mfes)
        return mfes
