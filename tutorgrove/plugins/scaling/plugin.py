from importlib import resources

from tutor import hooks, config as tutor_config

from ...hook_utils import merge_dict, bootstrap_plugin

try:
    from tutorpod_autoscaling.hooks import AUTOSCALING_CONFIG
except ImportError:
    AUTOSCALING_CONFIG = None

from ...__about__ import __version__

config = {
    # Add here your new settings
    "defaults": {
        "VERSION": __version__,
        # Pod autoscaling configs to be configured
        "AUTOSCALING_CONFIG": {},
        # Kubernetes Pod Disruption Budgets -- if set to -1 (not a string), then PDB is disabled
        # Max unavailable is set to 0 to prevent any downtime during upgrades, however this may
        # cause issues in case of many replicas. If the autoscaling is set to other than the
        # default, then the max unavailable should be considered to be set to a higher value.
        "CADDY_PDB_MAX_UNAVAILABLE": 0,
        "LMS_PDB_MAX_UNAVAILABLE": 0,
        "CMS_PDB_MAX_UNAVAILABLE": 0,
        "LMS_WORKER_PDB_MAX_UNAVAILABLE": 0,
        "CMS_WORKER_PDB_MAX_UNAVAILABLE": 0,
        "FORUM_PDB_MAX_UNAVAILABLE": 0,
        "MFE_PDB_MAX_UNAVAILABLE": 0,
    },
}

bootstrap_plugin("scaling", config)

if AUTOSCALING_CONFIG:
    @hooks.Actions.PROJECT_ROOT_READY.add()
    def _modify_autoscaling_configs(root: str):
        config = tutor_config.load_minimal(root)
        new_configs = config.get('GROVE_AUTOSCALING_CONFIG', {})

        @AUTOSCALING_CONFIG.add()
        def _modify_default_autoscaling_config(scaling_config):
            scaling_config = merge_dict(scaling_config, new_configs)
            return scaling_config
