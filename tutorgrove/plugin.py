from .plugins.mfes import plugin as mfe_plugin
from .plugins.scaling import plugin as scaling_plugin
from .plugins.elasticsearch import plugin as elasticsearch_plugin
from .plugins.tenants import plugin as tenants_plugin
from .plugins.config import plugin as config_plugin
