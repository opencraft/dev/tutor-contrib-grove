import io
import os
from setuptools import setup, find_packages

HERE = os.path.abspath(os.path.dirname(__file__))


def load_readme():
    with io.open(os.path.join(HERE, "README.rst"), "rt", encoding="utf8") as f:
        return f.read()


def load_about():
    about = {}
    with io.open(
        os.path.join(HERE, "tutorgrove", "__about__.py"),
        "rt",
        encoding="utf-8",
    ) as f:
        exec(f.read(), about)  # pylint: disable=exec-used
    return about


ABOUT = load_about()


setup(
    name="tutor-contrib-grove",
    version=ABOUT["__version__"],
    url="https://gitlab.com/opencraft/dev/tutor-contrib-grove",
    project_urls={
        "Code": "https://gitlab.com/opencraft/dev/tutor-contrib-grove",
        "Issue tracker": "https://gitlab.com/opencraft/dev/tutor-contrib-grove/issues",
    },
    license="AGPLv3",
    author="Kaustav Banerjee",
    description="grove plugin for Tutor",
    long_description=load_readme(),
    packages=find_packages(exclude=["tests*"]),
    include_package_data=True,
    python_requires=">=3.9",
    install_requires=["tutor"],
    entry_points={
        "tutor.plugin.v1": [
            "grove = tutorgrove.plugin",
            "grove-mfes = tutorgrove.plugins.mfes.plugin",
            "grove-scaling = tutorgrove.plugins.scaling.plugin",
            "grove-elasticsearch = tutorgrove.plugins.elasticsearch.plugin",
            "grove-config = tutorgrove.plugins.config.plugin",
            "grove-tenants = tutorgrove.plugins.tenants.plugin",
        ]
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
    ],
)
