grove plugins for `Tutor`_
==========================

These plugins is meant to be used with `Grove`_

They contains patches and commands useful for deployment of instances using Grove.

Here is the list of patches and commands:

1. Run ``create_or_update_site_configuration`` management command during
   lms init. This sets the site configurations for the grove instance.
   The parameters required are ``SITE_CONFIG`` and ``LMS_HOST``. `Site
   Configuration`_

Installation
------------

.. code:: bash

   pip install git+https://gitlab.com/opencraft/dev/tutor-contrib-grove

Usage
-----

This package is composed of multiple plugins:

- `grove-config`: For general configuration, setting LMS, and CMS variables,
   waffle flags, creating cron-jobs etc.
- `grove-elasticsearch`: For configuring a shared elasticsearch instance.
- `grove-mfes`: For adding new MFEs or disabling existing MFEs.
- `grove-scaling`: For some scaling-related config.
- `grove-tenants`: For supporting multi-tenancy.

There is also a meta-package `grove` that enables all plugins.

.. code:: bash

   tutor plugins enable grove

Releasing a new version
-----------------------

The versioning format used for this plugin is `RELEASE.MAJOR.MINOR`.

When releasing a new version, increment the:

- RELEASE version when a new Tutor version is released. The new value should match the Tutor release version.
- MAJOR version when there are new features.
- MINOR version when there are bug fixes.

Then follow these steps:

1. Bump the version in `tutorgrove/__about__.py`.
2. Create a new tag with the version number.
3. Bump this plugin's version in the [grove-template](https://gitlab.com/opencraft/dev/grove-template) repository.
4. Bump this plugin's version in the desired cluster's instances' `requirements.txt` file.
5. Bump this plugin's version in the desired cluster's [default instance's](https://gitlab.com/opencraft/dev/grove-template/-/blob/main/defaults/requirements.txt?ref_type=heads) `requirements` file.

Cron jobs
---------

This plugin allows you to run cron jobs by defining an array of objects
containing the job schedule and script to execute. The jobs are running using
the edxapp docker image, however not within the LMS/Studio containers.

There are many reasons you may want this behaviour, for example running
`manage.py` commands periodically, like aggregator cleanups. Take the following
config example:

.. code-block:: yaml

    # instance/config.yml
    GROVE_CRON_JOBS:
        - name: "run-aggregator-service"
          schedule: "*/10 * * * *"
          script: "./manage.py lms run_aggregator_service"
        - name: "hello-world"
          schedule: "*/20 * * * *"
          script: 'echo "Hello world"

Please note that only "-" and alphanumeric characters are allowed in name
field. Spaces and underscores are replaced by hypen i.e. "-" automatically.

Autoscaling
-----------

In order to harmonize the effort of Open edX providers, we decided to remove the
support of autoscaling from `tutor-contrib-grove`, in favour of
[tutor-contrib-pod-autoscaling](https://github.com/eduNEXT/tutor-contrib-pod-autoscaling).

Static URL
----------

Grove's AWS provider supports using a CDN for LMS and CMS static assets. To make use of the CDN, this plugin
exposes configuration variables to set the `STATIC_URL` for the LMS and CMS.

These are, respectively.

- `GROVE_LMS_STATIC_URL`
- `GROVE_CMS_STATIC_URL`

In addition when configuring MFE's the MFE origin needs to be configured.

- `GROVE_MFE_CDN_ORIGIN`


Multi-tenancy
-------------

Grove can deploy multi-tenant installations with a different theme for each instance. You can read more about this
in the [Grove documenation](https://grove.opencraft.com/addons/multi-domain-setups/).

License
-------

This software is licensed under the terms of the AGPLv3.

.. _Tutor: https://docs.tutor.overhang.io
.. _Grove: https://grove.opencraft.com
.. _Site Configuration: https://grove.opencraft.com/user-guides/overriding-configuration/#site-configuration

